@extends('layout.master')

@section('content')


    <!--SECTION: HEADER AND BANNER-->
    <section>
        <div class="home event">
            <div class="h_l">
                <!-- BRAND LOGO AND EVENT NAMES -->
                <img src="images/logo.png" alt="" />
                <h2>Next Event</h2>
                <p>Tournament selanjutnya dari junior dan senior di indonesia.</p>
                <ul>
                    <li><a href="soccer.html"><span>1</span>BNTP TENNIS CHAMPIONSHIP JAKARTA / SR</a>
                    </li>
                    <li><a href="cycling.html"><span>2</span>BNTP TENNIS CHAMPIONSHIP JAKARTA / SR</a>
                    </li>
                </ul>
                <a href="#" class="aebtn">View All</a>
            </div>
            <div class="h_r">
                <!-- SLIDER -->
                <div class="slideshow-container">
                    <!-- FIRST SLIDER -->
                    <div class="mySlides fade">
                        <div class="numbertext">1 / 2</div>
                        <a href="#"><img src="{{ asset('images/xample/banner1.jpg') }}" alt="">
                        </a>
                        <!--<div class="text">Caption Text</div>-->
                    </div>
                    <!-- SECOND SLIDER -->
                    <div class="mySlides fade">
                        <div class="numbertext">2 / 2</div>
                        <a href="#"><img src="{{ asset('images/xample/banner2.jpeg') }}" alt="">
                        </a>
                        <!--<div class="text">Caption Text</div>-->
                    </div>
                    <!-- YOU CAN ADD MULTIPLE SLIDERS NOW-->
                    <!-- SLIDER NAVIGATION -->
                    <a class="prev" onclick="plusSlides(-1)">❮</a>
                    <a class="next" onclick="plusSlides(1)">❯</a>
                </div>
            </div>
        </div>
    </section>

    <!--SECTION: UPCOMING SPORTS EVENTS-->
    <section>
        <div class="se lp">
            <div class="row info">
                <div class="col-md-6 col-sm-12 youtube">
                    <iframe width="527" height="315" src="https://www.youtube.com/embed/OzG9kGNcQXk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <a href="#" class="col-md-offset-4 col-md-4 aebtn">View All</a>
                </div>
                <div class="col-md-6 col-sm-12 rangking">
                    <h5>RANKING</h5>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <h5>Junior</h5>
                            <div class="table-responsive">
                                <table class="table table-striped center">
                                    <tr>
                                        <td colspan="3">KU 18 (Putra)</td>
                                    </tr>
                                    <tr class="head">
                                        <td>Rank</td>
                                        <td>Nama</td>
                                        <td>Point</td>
                                    </tr>
                                    <tr class="body">
                                        <td>1</td>
                                        <td>Bambang</td>
                                        <td>3000</td>
                                    </tr>
                                    <tr class="body">
                                        <td>2</td>
                                        <td>Andi</td>
                                        <td>2000</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">KU 18 (Putri)</td>
                                    </tr>
                                    <tr class="head">
                                        <td>Rank</td>
                                        <td>Nama</td>
                                        <td>Point</td>
                                    </tr>
                                    <tr class="body">
                                        <td>1</td>
                                        <td>Wanti</td>
                                        <td>3000</td>
                                    </tr>
                                    <tr class="body">
                                        <td>2</td>
                                        <td>Rosa</td>
                                        <td>2000</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <h5>Umum</h5>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tr class="head">
                                        <td>Rank</td>
                                        <td>Nama</td>
                                        <td>Point</td>
                                    </tr>
                                    <tr class="body">
                                        <td>1</td>
                                        <td>Bambang</td>
                                        <td>3000</td>
                                    </tr>
                                    <tr class="body">
                                        <td>2</td>
                                        <td>Andi</td>
                                        <td>2000</td>
                                    </tr>
                                    <tr class="body">
                                        <td>3</td>
                                        <td>Budi</td>
                                        <td>2000</td>
                                    </tr>
                                    <tr class="body">
                                        <td>4</td>
                                        <td>Jajang</td>
                                        <td>2000</td>
                                    </tr>
                                    <tr class="body">
                                        <td>5</td>
                                        <td>Waluyo</td>
                                        <td>2000</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="col-md-offset-4 col-md-4 aebtn">View All</a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row news">
                <!-- LEFT SIDE: SPORTS EVENTS -->
                <div class="col-sm-12 col-md-6">
                    <!--Sports Events in Dubai-->
                    <ul>
                        <!-- SPORTS EVENT:1 -->
                        <li>
                            <div class="el-img">
                                <img class="img-responsive" src="https://pelti.or.id/wp-content/uploads/2019/02/DSC08601-360x240.jpg" alt="">
                            </div>
                            <div class="el-con">
                                <span>3 MAY, 2019</span>
                                <h4>PELTI Kenalkan Tenis Mulai Dari Sekolah Dasar</h4>
                                <a href="#">Read More</a>
                            </div>
                        </li>
                        <li>
                            <div class="el-img">
                                <img class="img-responsive" src="https://pelti.or.id/wp-content/uploads/2019/02/BannerBerDaerah-1-360x240.jpg" alt="">
                            </div>
                            <div class="el-con">
                                <span>1 FEBRUARY, 2019</span>
                                <h4>Lahirnya Weekend Yunior Tenis Circuit</h4>
                                <a href="#">Read More</a>
                            </div>
                        </li>
                        <li>
                            <div class="el-img">
                                <img class="img-responsive" src="https://pelti.or.id/wp-content/uploads/2019/02/DSC08601-360x240.jpg" alt="">
                            </div>
                            <div class="el-con">
                                <span>3 MAY, 2019</span>
                                <h4>PELTI Kenalkan Tenis Mulai Dari Sekolah Dasar</h4>
                                <a href="#">Read More</a>
                            </div>
                        </li>
                        <li>
                            <div class="el-img">
                                <img class="img-responsive" src="https://pelti.or.id/wp-content/uploads/2019/02/BannerBerDaerah-1-360x240.jpg" alt="">
                            </div>
                            <div class="el-con">
                                <span>1 FEBRUARY, 2019</span>
                                <h4>Lahirnya Weekend Yunior Tenis Circuit</h4>
                                <a href="#">Read More</a>
                            </div>
                        </li>

                    </ul>
                </div>
                <div class="col-sm-12 col-md-6">
                    <ul>
                        <!-- SPORTS EVENT:1 -->
                        <li>
                            <div class="el-img">
                                <img class="img-responsive" src="https://pelti.or.id/wp-content/uploads/2019/02/DSC08601-360x240.jpg" alt="">
                            </div>
                            <div class="el-con">
                                <span>3 MAY, 2019</span>
                                <h4>PELTI Kenalkan Tenis Mulai Dari Sekolah Dasar</h4>
                                <a href="#">Read More</a>
                            </div>
                        </li>
                        <li>
                            <div class="el-img">
                                <img class="img-responsive" src="https://pelti.or.id/wp-content/uploads/2019/02/BannerBerDaerah-1-360x240.jpg" alt="">
                            </div>
                            <div class="el-con">
                                <span>1 FEBRUARY, 2019</span>
                                <h4>Lahirnya Weekend Yunior Tenis Circuit</h4>
                                <a href="#">Read More</a>
                            </div>
                        </li>
                        <li>
                            <div class="el-img">
                                <img class="img-responsive" src="https://pelti.or.id/wp-content/uploads/2019/02/DSC08601-360x240.jpg" alt="">
                            </div>
                            <div class="el-con">
                                <span>3 MAY, 2019</span>
                                <h4>PELTI Kenalkan Tenis Mulai Dari Sekolah Dasar</h4>
                                <a href="#">Read More</a>
                            </div>
                        </li>
                        <li>
                            <div class="el-img">
                                <img class="img-responsive" src="https://pelti.or.id/wp-content/uploads/2019/02/BannerBerDaerah-1-360x240.jpg" alt="">
                            </div>
                            <div class="el-con">
                                <span>1 FEBRUARY, 2019</span>
                                <h4>Lahirnya Weekend Yunior Tenis Circuit</h4>
                                <a href="#">Read More</a>
                            </div>
                        </li>

                    </ul>
                </div>
                <a href="#" class="col-md-offset-5 col-md-2 aebtn">View All</a>
            </div>
        </div>
    </section>

    <!--SECTION: FOOTER-->
    <section>
        <div class="ffoot">
            <div class="lp">
                <!--SECTION: FOOTER-->
                <div class="row">
                    <div class="col-md-12 foot1">
                        <a href="#"><img src="images/logo.png" alt="">
                        </a>
                        <ul>
                            <li><span>10,231,124</span> Community Members</li>
                            <li><span>512</span> Events</li>
                            <li><span>2124</span> News</li>
                        </ul>
                    </div>
                </div>
                <!--SECTION: FOOTER-->
                <div class="row foot2">
                    <div class="col-md-3">
                        <div class="foot2-1 foot-com">
                            <h4>VISI</h4>
                            <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A. Landmark : Next To Airport</p>
                        </div>
                        <div class="foot2-1 foot-com">
                            <h4>MISI</h4>
                            <p>No 3241, Grandiz Street Towers, Desay City, U.A.E. Landmark : Next To Grand Hotel</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="foot2-1 foot-com">
                            <h4>LATAR BELAKANG</h4>
                            <p>No 3241, Grandiz Street Towers, Desay City, U.A.E. Landmark : Next To Grand Hotel</p>
                        </div>
                        <div class="foot2-2 foot-soc foot-com">
                            <h4>Follow Us Now</h4>
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook fb1"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-twitter tw1"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-google-plus gp1"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-whatsapp wa1"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-envelope-o sh1"></i></a>
                                </li>
                            </ul>
                            <span class="foot-ph">Phone: +68 215707203</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="foot2-32 foot-pop foot-com">
                            <h4>MAP</h4>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3642.9719549633164!2d106.80079240702739!3d-6.220336860778754!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f14b5e389f4f%3A0x5bfd802500992d10!2sTennis+Indoor+Stadium!5e0!3m2!1sid!2sid!4v1558972920352!5m2!1sid!2sid" width="527" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @endsection('content')
