@extends('layout.master')

@section('content')

@include('layout.header-search')

    <!-- SECTION: TRAINING -->
    <section>
        <div class="training img-pag-about">
            <div class="tr-pro">
                <div class="inn-title">
                    <h2><i class="fa fa-check" aria-hidden="true"></i> sports 2019 <span> about us</span></h2>
                    <p>Becoming a gym certified personal fitness trainer is your foundation for success. gym is the only personal trainer certification program that integrates</p>
                </div>
                <!-- TRAINING BENEFITS -->
                <div class="inn-all-com">
                    <h4>match Start Date</h4>
                    <p>Becoming a gym certified personal fitness trainer is your foundation for success. gym is the only personal trainer certification program that integrates a complete approach to fitness, wellness and business skills.</p>
                    <div class="inn-ev-date">
                        <div class="inn-ev-date-left">
                            <h4>28 th</h4>
                            <span>augest 2019</span>
                        </div>
                        <div class="inn-ev-date-rig">
                            <ul>
                                <li>20 <span>days</span>
                                </li>
                                <li>08 <span>hours</span>
                                </li>
                                <li>35 <span>min</span>
                                </li>
                                <li>47 <span>sec</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- TRAINING BENEFITS -->
                <div class="inn-all-com inn-all-list tp-1">
                    <h4>Other Details</h4>
                    <p>Becoming a gym certified personal fitness trainer is your foundation for success. gym is the only personal trainer certification program that integrates a complete approach to fitness, wellness and business skills.</p>
                    <ul>
                        <li>Get trained by qualified personnel</li>
                        <li>Guest lectures by International faculty</li>
                        <li>Internship with the Global fitness leader</li>
                        <li>Placement opportunities with Gold’s Gym</li>
                        <li>Earn handsome salaries on completion of course</li>
                        <li>Fitness Assessment room</li>
                    </ul>
                </div>
                <!-- TRAINING BENEFITS -->
                <div class="inn-all-com inn-all-list inn-pad-top-5 tp-1">
                    <h4>Current match events</h4>
                    <p>Becoming a gym certified personal fitness trainer is your foundation for success. gym is the only personal trainer certification program that integrates a complete approach to fitness, wellness and business skills.</p>
                    <a href="#" class="inn-te-ra-link">Click to view Current match events</a>
                </div>
                <!-- TRAINING BENEFITS -->
                <div class="inn-all-com inn-all-list inn-pad-top-5 tp-1">
                    <h4>Upcoming sports events</h4>
                    <p>Becoming a gym certified personal fitness trainer is your foundation for success. gym is the only personal trainer certification program that integrates a complete approach to fitness, wellness and business skills.</p>
                    <a href="#" class="inn-te-ra-link">Click to view Upcoming sports events</a>
                </div>
                <!-- TRAINING BENEFITS -->
                <div class="inn-all-com inn-all-list inn-pad-top-5 tp-1">
                    <h4>Top trending sports events</h4>
                    <p>Becoming a gym certified personal fitness trainer is your foundation for success. gym is the only personal trainer certification program that integrates a complete approach to fitness, wellness and business skills.</p>
                    <a href="#" class="inn-te-ra-link">Click to view Top trending sports</a>
                </div>
                <!-- TRAINING BENEFITS -->
                <div class="inn-all-com inn-all-list inn-pad-top-5 tp-1">
                    <h4>Ticket Booking</h4>
                    <p>Becoming a gym certified personal fitness trainer is your foundation for success. gym is the only personal trainer certification program that integrates a complete approach to fitness, wellness and business skills.</p>
                    <a href="#" class="inn-te-ra-link">Click to Book your tickets</a>
                </div>
                <!-- TRAINING BENEFITS -->
                <div class="inn-all-com inn-all-list inn-pad-top-5 tp-1">
                    <h4>Team Registration</h4>
                    <p>Becoming a gym certified personal fitness trainer is your foundation for success. gym is the only personal trainer certification program that integrates a complete approach to fitness, wellness and business skills.</p>
                    <a href="#" class="inn-te-ra-link">Click to Register your Team</a>
                </div>
                <!-- TRAINING BENEFITS -->
                <div class="inn-all-com inn-all-list inn-pad-top-5 tp-1">
                    <h4>Team Ranking</h4>
                    <p>Becoming a gym certified personal fitness trainer is your foundation for success. gym is the only personal trainer certification program that integrates a complete approach to fitness, wellness and business skills.</p>
                    <a href="#" class="inn-te-ra-link">Click to view Team Ranking</a>
                </div>
                <!-- TRAINING BENEFITS -->
                <div class="inn-all-com inn-all-list inn-pad-top-5 tp-1">
                    <h4>OUR SPONSORS</h4>
                    <p>Becoming a gym certified personal fitness trainer is your foundation for success. gym is the only personal trainer certification program that integrates a complete approach to fitness, wellness and business skills.</p>
                    <a href="#" class="inn-te-ra-link">Click to Sponsors</a>
                </div>
                <!-- TRAINING BENEFITS -->
                <div class="inn-all-com inn-all-list inn-pad-top-5 tp-1">
                    <h4>make donation</h4>
                    <p>Becoming a gym certified personal fitness trainer is your foundation for success. gym is the only personal trainer certification program that integrates a complete approach to fitness, wellness and business skills.</p>
                    <a href="#" class="inn-te-ra-link">Click to Donate</a>
                </div>
            </div>
        </div>
    </section>
    <!--SECTION: BLOG POSTS-->
    <section>
        <div class="blog row">
            <div class="lp">
                <!-- BLOG POST: POST DATE -->
                <div class="blog-1 col-md-2">
                    <span>Latest Posts</span>
                    <h4>25</h4>
                    <span>Augest 2017</span>
                </div>
                <!-- BLOG POST: POST NAME & DESCRIPTION -->
                <div class="blog-2 col-md-8">
                    <ul>
                        <li>
                            <a href="#">
                                <h4>WESTERN SYDNEY WANDERERS VS URAWA RED DIAMONDS</h4>
                            </a>
                        </li>
                        <li>
                            <p>In efficitur nisi et condimentum mattis. Duis et aliquet purus, quis congue elit. Cras volutpat dapibus molestie. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec posuere mollis augue, a accumsan libero egestas sit amet.Vestibulum posuere erat tortor, porta tempus leo condimentum sed. </p>
                        </li>
                    </ul>
                </div>
                <!-- BLOG POST: POST COMMENTS,TAG AND SOCIAL MEDIA -->
                <div class="blog-3 col-md-2">
                    <ul>
                        <li><i class="fa fa-comment-o" aria-hidden="true"></i> Comments</li>
                        <li><i class="fa fa-tag" aria-hidden="true"></i> Tag</li>
                        <li><i class="fa fa-share-alt" aria-hidden="true"></i> Share This</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--SECTION: FOOTER-->
@include('layout.footer')
@endsection