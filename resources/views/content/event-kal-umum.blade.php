@extends('layout.master')

@section('content')
include('header-search')
    <!-- REGISTER MY INFORMATION -->
    <section>
        <div class="booking-bg-s lp">
            <div class="booking-bg-1">
                <!-- <nav>
                    <ol class="cd-multi-steps text-bottom count">
                        <li class="visited"><a href="event-register.html">Register Tournament</a></li>
                        <li class="current"><a href="event-kal-junior.html">Kalender Penyelenggara</a></li>
                        <li><em>Size of Draw</em></li>
                        <li><em>Lokasi Pertandingan</em></li>
                        <li><em>Fasilitas Penunjang</em></li>
                        <li><em>Perangkat Pertandingan</em></li>
                        <li><em>Info Pendaftaran</em></li>
                        <li><em>Biaya Pendaftaran</em></li>
                        <li><em>Konfirmasi Pembayaran</em></li>
                    </ol>
                </nav> -->
                <div class="bg-book">
                    <div class="spe-title-1 spe-title-wid">
                        <!-- <h2>Register your <span>Event Now!</span> </h2> -->
                        <h2>Kalender <span>Penyelengaraan</span> </h2>
                        <div class="hom-tit">
                            <div class="hom-tit-1"></div>
                            <div class="hom-tit-2"></div>
                            <div class="hom-tit-3"></div>
                        </div>
                        <!-- <p>Feel the thrill of seeing a global sporting event in one of the world's most incredible cities. Headlining the calendar is the Dubai World Cup</p> -->
                    </div>
                    <!-- <div class="book-succ">Thank you for Register your Event with us.</div> -->
                    <div class="book-form">
                        <form class="form-horizontal" id="er_form" name="er_form" action="mail/er.php">
                            <div class="form-group">
                                <div class="col-sm-10">
                                    <label class="radio-inline"><input type="radio" name="jenis" checked>Umum</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- Putra -->
                                <label class="control-label col-sm-12 left" style="text-align: center; font-size: 18px;">Putra</label>
                                <div class="clearfix"></div>
                                <div style="margin: 10px 0px;">
                                    <label class="control-label col-sm-3" style="text-align: left;">Kualifikasi</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                    </div>
                                    <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div style="margin: 10px 0px;">
                                    <label class="control-label col-sm-3" style="text-align: left;">Babak Utama</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                    </div>
                                    <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                    </div>
                                </div>
                                <!-- Putri -->
                                <label class="control-label col-sm-12 left" style="text-align: center; font-size: 18px;">Putri</label>
                                <div class="clearfix"></div>
                                <div style="margin: 10px 0px;">
                                    <label class="control-label col-sm-3" style="text-align: left;">Kualifikasi</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                    </div>
                                    <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div style="margin: 10px 0px;">
                                    <label class="control-label col-sm-3" style="text-align: left;">Babak Utama</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                    </div>
                                    <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-offset-6 col-sm-3">
                                    <input type="submit" id="back" value="Back">
                                </div>
                                <div class="col-sm-3">
                                    <input type="submit" id="next" value="Next">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--SECTION: BLOG POSTS-->
   
    <script>
        $(document).ready(() => {
            $('#next').on('click', (e) => {
                window.location = "{{ url('/event-register/event-kal-umum/event-size-umum') }}";
            })
            $('#back').on('click', (e) => {
                window.location = "{{ ('/event-register/event-kal-umum') }}";
            })
        });
    </script>
@include('layout.footer')
@endsection