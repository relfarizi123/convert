<!DOCTYPE html>
<html lang="en">

<head>
    <title>Real Sports Club: Soccer,Football,Cricket,Water Sports,Fitness,Yoga and All Sports Events</title>
    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- FAV ICON(BROWSER TAB ICON) -->
    <link rel="shortcut icon" href="images/fav.ico" type="image/x-icon">

    <!-- GOOGLE FONT -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700%7CMontserrat:300,500%7COswald:400,500" rel="stylesheet">

    <!-- FONTAWESOME ICONS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!-- ALL CSS FILES -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/custome.css">

    <!-- MOB.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
    <link rel="stylesheet" href="css/mob.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>

    <section>
        <!-- LEFT SIDE NAVIGATION MENU -->
        <div class="menu">
                <ul>
                    <!-- MAIN MENU -->
                    <li>
                        <a href="index.html"><img src= "assets/images/iconzzyy/s1.png" alt=""> Home</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/menu/about.png" alt=""> About</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/menu/turnament.png" alt=""> Tournament</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/menu/rangking.png" alt=""> Rangking</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/menu/player.png" alt=""> Player</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/menu/news.png" alt=""> News</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/menu/galerry.png" alt=""> Gallery</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/menu/ayotennis.png" alt=""> Ayo Tennis</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/menu/club.png" alt=""> Club</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/contact.png" alt=""> Contact US</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/about.png" alt=""> Login</a>
                    </li>
                </ul>
        </div>

        <!-- RIGHT SIDE NAVIGATION MENU -->
        <!-- MOBILE MENU(This mobile menu show on 0px to 767px windows only) -->
        <div class="mob-menu">
            <span><i class="fa fa-bars" aria-hidden="true"></i></span>
        </div>
        <div class="mob-close">
            <span><i class="fa fa-times" aria-hidden="true"></i></span>
        </div>
    </section>

    <!-- TOP BAR -PHONE EMAIL AND TOP MENU -->
    <section class="i-head-top">
        <div class="i-head row">
            <!-- TOP CONTACT INFO -->
            <div class="i-head-left i-head-com col-md-6">
                <ul>
                    <li><a href="#">phone: +62 6514 7415</a>
                    </li>
                    <li><a href="#">Email: sports@dnatechnology.co.id</a>
                    </li>
                </ul>
            </div>
            <!-- TOP FIXED MENU -->
            <div class="i-head-right i-head-com col-md-6 col-sm-12 col-xs-12">
                <ul>
                    <li class="top-scal"><a href="booking.html"><i class="fa fa-ticket" aria-hidden="true"></i> Ticket Booking</a>
                    </li>
                    <li class="top-scal-1"><a href="event-register"><i class="fa fa-registered" aria-hidden="true"></i> Event Register</a>
                    </li>
                    <li><a href="#" class="tr-menu"><i class="fa fa-chevron-down" aria-hidden="true"></i> Browse</a>
                        <div class="cat-menu">

                            <div class="col-md-3 col-sm-6 cm1 mob-hid">
                                <h4>Popular Sports Category</h4>
                                <ul>
                                    <li><a href="football.html"><i class="fa fa-angle-right" aria-hidden="true"></i> american football</a>
                                    </li>
                                    <li><a href="soccer.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Soccer davidson</a>
                                    </li>
                                    <li><a href="cricket.html"><i class="fa fa-angle-right" aria-hidden="true"></i> cricket league</a>
                                    </li>
                                    <li><a href="tennis.html"><i class="fa fa-angle-right" aria-hidden="true"></i> tennis davidson</a>
                                    </li>
                                    <li><a href="volleyball.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Volleyball Champions</a>
                                    </li>
                                    <li><a href="training.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Fitness Training</a>
                                    </li>
                                    <li><a href="all-sports.html"><i class="fa fa-angle-right" aria-hidden="true"></i> View All Sports</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3 col-sm-6 cm1 mob-hid">
                                <h4>current events</h4>
                                <ul>
                                    <li><a href="soccer.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Soccer Coaching Under 18</a>
                                    </li>
                                    <li><a href="yoga.html"><i class="fa fa-angle-right" aria-hidden="true"></i> yoga & fitness training classes</a>
                                    </li>
                                    <li><a href="body-building.html"><i class="fa fa-angle-right" aria-hidden="true"></i> bodybuilding 2019</a>
                                    </li>
                                    <li><a href="surfing.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Hawaii Surfing Championship</a>
                                    </li>
                                    <li><a href="golf.html"><i class="fa fa-angle-right" aria-hidden="true"></i> european golf 2019 </a>
                                    </li>
                                    <li><a href="cricket.html"><i class="fa fa-angle-right" aria-hidden="true"></i> cricket champions league</a>
                                    </li>
                                    <li><a href="events.html"><i class="fa fa-angle-right" aria-hidden="true"></i> View All Events</a>
                                    </li>
                                </ul>

                            </div>
                            <div class="col-md-6">
                                <h4>2019 upcoming league matches</h4>
                                <div class="foot-pop top-me-ev">
                                    <ul>
                                        <li>
                                            <a href="football.html">
                                                <img src="images/trends/2.jpeg" alt="">
                                                <div class="foot-pop-eve top-me-text">
                                                    <span>Soccer: AUGUST 23RD, 2019</span>
                                                    <h4>Football:THIS SATURDAY STARTS THE INTENSIVE TRAINING FOR THE FINAL,NEW YORK</h4>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="body-building.html">
                                                <img src="images/trends/5.jpg" alt="">
                                                <div class="foot-pop-eve top-me-text">
                                                    <span>BoduBuilding: AUGUST 23RD, 2019</span>
                                                    <h4>BoduBuilding:JAKE DRIBBLER ANNOUNCED THAT HE IS RETIRING NEXT MNONTH</h4>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="tennis.html">
                                                <img src="images/trends/6.jpeg" alt="">
                                                <div class="foot-pop-eve top-me-text">
                                                    <span>BASKETBALL: AUGUST 23RD, 2019</span>
                                                    <h4>BASKETBALL:THE ALCHEMISTS NEWS COACH IS BRINGIN A NEW SHOOTING GUARD</h4>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                            <div class="cbb2-home-nav-bot mob-hid">
                                <ul>
                                    <li>One of the  Best Sports website template forever <span>Call us on: +01 3214 6581</span>
                                    </li>
                                    <li><a href="javascript:void(0);" class="cbb2-ani-btn-join"><i class="fa fa-user" aria-hidden="true"></i> Account</a>
                                    </li>
                                    <li>
                                        <a href="join-our-club.html" class="cbb2-ani-btn"><i class="fa fa-life-buoy" aria-hidden="true"></i> Join Our Club</a>
                                    </li>
                                    <li><a href="join-club.html" class="cbb2-ani-btn"><i class="fa fa-dollar" aria-hidden="true"></i> Make Donation</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </li>
                </ul>
            </div>
        </div>
    </section>

    <!--SECTION: HEADER AND BANNER-->
    <section>
        <div class="home event">
            <div class="h_l">
                <!-- BRAND LOGO AND EVENT NAMES -->
                <img src="images/logo.png" alt="" />
                <h2>Next Event</h2>
                <p>Tournament selanjutnya dari junior dan senior di indonesia.</p>
                <ul>
                    <li><a href="soccer.html"><span>1</span>BNTP TENNIS CHAMPIONSHIP JAKARTA / SR</a>
                    </li>
                    <li><a href="cycling.html"><span>2</span>BNTP TENNIS CHAMPIONSHIP JAKARTA / SR</a>
                    </li>
                </ul>
                <a href="#" class="aebtn">View All</a>
            </div>
            <div class="h_r">
                <!-- SLIDER -->
                <div class="slideshow-container">
                    <!-- FIRST SLIDER -->
                    <div class="mySlides fade">
                        <div class="numbertext">1 / 2</div>
                        <a href="#"><img src="images/xample/banner1.jpg" alt="">
                        </a>
                        <!--<div class="text">Caption Text</div>-->
                    </div>
                    <!-- SECOND SLIDER -->
                    <div class="mySlides fade">
                        <div class="numbertext">2 / 2</div>
                        <a href="#"><img src="images/xample/banner2.jpeg" alt="">
                        </a>
                        <!--<div class="text">Caption Text</div>-->
                    </div>
                    <!-- YOU CAN ADD MULTIPLE SLIDERS NOW-->
                    <!-- SLIDER NAVIGATION -->
                    <a class="prev" onclick="plusSlides(-1)">❮</a>
                    <a class="next" onclick="plusSlides(1)">❯</a>
                </div>
            </div>
        </div>
    </section>

    <!--SECTION: UPCOMING SPORTS EVENTS-->
    <section>
        <div class="se lp">
            <div class="row info">
                <div class="col-md-6 col-sm-12 youtube">
                    <iframe width="527" height="315" src="https://www.youtube.com/embed/OzG9kGNcQXk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <a href="#" class="col-md-offset-4 col-md-4 aebtn">View All</a>
                </div>
                <div class="col-md-6 col-sm-12 rangking">
                    <h5>RANKING</h5>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <h5>Junior</h5>
                            <div class="table-responsive">
                                <table class="table table-striped center">
                                    <tr>
                                        <td colspan="3">KU 18 (Putra)</td>
                                    </tr>
                                    <tr class="head">
                                        <td>Rank</td>
                                        <td>Nama</td>
                                        <td>Point</td>
                                    </tr>
                                    <tr class="body">
                                        <td>1</td>
                                        <td>Bambang</td>
                                        <td>3000</td>
                                    </tr>
                                    <tr class="body">
                                        <td>2</td>
                                        <td>Andi</td>
                                        <td>2000</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">KU 18 (Putri)</td>
                                    </tr>
                                    <tr class="head">
                                        <td>Rank</td>
                                        <td>Nama</td>
                                        <td>Point</td>
                                    </tr>
                                    <tr class="body">
                                        <td>1</td>
                                        <td>Wanti</td>
                                        <td>3000</td>
                                    </tr>
                                    <tr class="body">
                                        <td>2</td>
                                        <td>Rosa</td>
                                        <td>2000</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <h5>Umum</h5>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tr class="head">
                                        <td>Rank</td>
                                        <td>Nama</td>
                                        <td>Point</td>
                                    </tr>
                                    <tr class="body">
                                        <td>1</td>
                                        <td>Bambang</td>
                                        <td>3000</td>
                                    </tr>
                                    <tr class="body">
                                        <td>2</td>
                                        <td>Andi</td>
                                        <td>2000</td>
                                    </tr>
                                    <tr class="body">
                                        <td>3</td>
                                        <td>Budi</td>
                                        <td>2000</td>
                                    </tr>
                                    <tr class="body">
                                        <td>4</td>
                                        <td>Jajang</td>
                                        <td>2000</td>
                                    </tr>
                                    <tr class="body">
                                        <td>5</td>
                                        <td>Waluyo</td>
                                        <td>2000</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="col-md-offset-4 col-md-4 aebtn">View All</a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row news">
                <!-- LEFT SIDE: SPORTS EVENTS -->
                <div class="col-sm-12 col-md-6">
                    <!--Sports Events in Dubai-->
                    <ul>
                        <!-- SPORTS EVENT:1 -->
                        <li>
                            <div class="el-img">
                                <img class="img-responsive" src="https://pelti.or.id/wp-content/uploads/2019/02/DSC08601-360x240.jpg" alt="">
                            </div>
                            <div class="el-con">
                                <span>3 MAY, 2019</span>
                                <h4>PELTI Kenalkan Tenis Mulai Dari Sekolah Dasar</h4>
                                <a href="#">Read More</a>
                            </div>
                        </li>
                        <li>
                            <div class="el-img">
                                <img class="img-responsive" src="https://pelti.or.id/wp-content/uploads/2019/02/BannerBerDaerah-1-360x240.jpg" alt="">
                            </div>
                            <div class="el-con">
                                <span>1 FEBRUARY, 2019</span>
                                <h4>Lahirnya Weekend Yunior Tenis Circuit</h4>
                                <a href="#">Read More</a>
                            </div>
                        </li>
                        <li>
                            <div class="el-img">
                                <img class="img-responsive" src="https://pelti.or.id/wp-content/uploads/2019/02/DSC08601-360x240.jpg" alt="">
                            </div>
                            <div class="el-con">
                                <span>3 MAY, 2019</span>
                                <h4>PELTI Kenalkan Tenis Mulai Dari Sekolah Dasar</h4>
                                <a href="#">Read More</a>
                            </div>
                        </li>
                        <li>
                            <div class="el-img">
                                <img class="img-responsive" src="https://pelti.or.id/wp-content/uploads/2019/02/BannerBerDaerah-1-360x240.jpg" alt="">
                            </div>
                            <div class="el-con">
                                <span>1 FEBRUARY, 2019</span>
                                <h4>Lahirnya Weekend Yunior Tenis Circuit</h4>
                                <a href="#">Read More</a>
                            </div>
                        </li>

                    </ul>
                </div>
                <div class="col-sm-12 col-md-6">
                    <ul>
                        <!-- SPORTS EVENT:1 -->
                        <li>
                            <div class="el-img">
                                <img class="img-responsive" src="https://pelti.or.id/wp-content/uploads/2019/02/DSC08601-360x240.jpg" alt="">
                            </div>
                            <div class="el-con">
                                <span>3 MAY, 2019</span>
                                <h4>PELTI Kenalkan Tenis Mulai Dari Sekolah Dasar</h4>
                                <a href="#">Read More</a>
                            </div>
                        </li>
                        <li>
                            <div class="el-img">
                                <img class="img-responsive" src="https://pelti.or.id/wp-content/uploads/2019/02/BannerBerDaerah-1-360x240.jpg" alt="">
                            </div>
                            <div class="el-con">
                                <span>1 FEBRUARY, 2019</span>
                                <h4>Lahirnya Weekend Yunior Tenis Circuit</h4>
                                <a href="#">Read More</a>
                            </div>
                        </li>
                        <li>
                            <div class="el-img">
                                <img class="img-responsive" src="https://pelti.or.id/wp-content/uploads/2019/02/DSC08601-360x240.jpg" alt="">
                            </div>
                            <div class="el-con">
                                <span>3 MAY, 2019</span>
                                <h4>PELTI Kenalkan Tenis Mulai Dari Sekolah Dasar</h4>
                                <a href="#">Read More</a>
                            </div>
                        </li>
                        <li>
                            <div class="el-img">
                                <img class="img-responsive" src="https://pelti.or.id/wp-content/uploads/2019/02/BannerBerDaerah-1-360x240.jpg" alt="">
                            </div>
                            <div class="el-con">
                                <span>1 FEBRUARY, 2019</span>
                                <h4>Lahirnya Weekend Yunior Tenis Circuit</h4>
                                <a href="#">Read More</a>
                            </div>
                        </li>

                    </ul>
                </div>
                <a href="#" class="col-md-offset-5 col-md-2 aebtn">View All</a>
            </div>
        </div>
    </section>

    <!--SECTION: FOOTER-->
    <section>
        <div class="ffoot">
            <div class="lp">
                <!--SECTION: FOOTER-->
                <div class="row">
                    <div class="col-md-12 foot1">
                        <a href="#"><img src="images/logo.png" alt="">
                        </a>
                        <ul>
                            <li><span>10,231,124</span> Community Members</li>
                            <li><span>512</span> Events</li>
                            <li><span>2124</span> News</li>
                        </ul>
                    </div>
                </div>
                <!--SECTION: FOOTER-->
                <div class="row foot2">
                    <div class="col-md-3">
                        <div class="foot2-1 foot-com">
                            <h4>VISI</h4>
                            <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A. Landmark : Next To Airport</p>
                        </div>
                        <div class="foot2-1 foot-com">
                            <h4>MISI</h4>
                            <p>No 3241, Grandiz Street Towers, Desay City, U.A.E. Landmark : Next To Grand Hotel</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="foot2-1 foot-com">
                            <h4>LATAR BELAKANG</h4>
                            <p>No 3241, Grandiz Street Towers, Desay City, U.A.E. Landmark : Next To Grand Hotel</p>
                        </div>
                        <div class="foot2-2 foot-soc foot-com">
                            <h4>Follow Us Now</h4>
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook fb1"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-twitter tw1"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-google-plus gp1"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-whatsapp wa1"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-envelope-o sh1"></i></a>
                                </li>
                            </ul>
                            <span class="foot-ph">Phone: +68 215707203</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="foot2-32 foot-pop foot-com">
                            <h4>MAP</h4>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3642.9719549633164!2d106.80079240702739!3d-6.220336860778754!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f14b5e389f4f%3A0x5bfd802500992d10!2sTennis+Indoor+Stadium!5e0!3m2!1sid!2sid!4v1558972920352!5m2!1sid!2sid" width="527" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Footer-->
    <!-- Sponsor -->
    <section>
        <div class="ffoot sponsor">
            <div class="lp">
                <div class="row">
                    <div class="col-md-12 foot4">
                        <h5>Our Sponsors</h5>
                        <ul>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-bri.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-totalindo.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-kapalapi.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-wika.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-harumenergy.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-abipraya.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-head.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-pp.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-bni.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-widyachandra.jpg" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <!-- FOOTER: COPY RIGHTS -->
        <div class="fcopy">
            <div class="lp copy-ri row">
                <div class="col-md-6 col-sm-12 col-xs-12">Supported by DNA TECHNOLOGY</div>
            </div>
        </div>
    </section>
    <!--== Bootstrap & Latest JS ==-->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>