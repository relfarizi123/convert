<!DOCTYPE html>
<html lang="en">

<head>
    <title>Real Sports Club: Soccer,Football,Cricket,Water Sports,Fitness,Yoga and All Sports Events</title>
    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- FAV ICON(BROWSER TAB ICON) -->
    <link rel="shortcut icon" href="images/fav.ico" type="image/x-icon">

    <!-- GOOGLE FONT -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700%7CMontserrat:300,500%7COswald:400,500" rel="stylesheet">

    <!-- FONTAWESOME ICONS -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!-- ALL CSS FILES -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/custome.css">
    <link rel="stylesheet" href="css/bootstrap.css">

    <!-- MOB.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
    <link rel="stylesheet" href="css/mob.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
    <![endif]-->
    
    <style>
        .radio-inline input, .checkbox-inline input { height: auto; }
    </style>

</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>

    <!--SECTION: LEFT MENU-->
    <section>
        <!-- LEFT SIDE NAVIGATION MENU -->
        <div class="menu">
                <ul>
                    <!-- MAIN MENU -->
                    <li>
                        <a href="index.html"><img src="images/icon/s1.png" alt=""> Home</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/menu/about.png" alt=""> About</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/menu/turnament.png" alt=""> Tournament</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/menu/rangking.png" alt=""> Rangking</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/menu/player.png" alt=""> Player</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/menu/news.png" alt=""> News</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/menu/galerry.png" alt=""> Gallery</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/menu/ayotennis.png" alt=""> Ayo Tennis</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/menu/club.png" alt=""> Club</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/contact.png" alt=""> Contact US</a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icon/about.png" alt=""> Login</a>
                    </li>
                </ul>
        </div>

        <!-- RIGHT SIDE NAVIGATION MENU -->
        <!-- MOBILE MENU(This mobile menu show on 0px to 767px windows only) -->
        <div class="mob-menu">
            <span><i class="fa fa-bars" aria-hidden="true"></i></span>
        </div>
        <div class="mob-close">
            <span><i class="fa fa-times" aria-hidden="true"></i></span>
        </div>
    </section>

    <!-- TOP BAR -PHPNE EMAIL AND TOP MENU -->
    <section class="i-head-top">
        <div class="i-head row">
            <!-- TOP CONTACT INFO -->
            <div class="i-head-left i-head-com col-md-6">
                <ul>
                    <li><a href="#">phone: +62 6514 7415</a>
                    </li>
                    <li><a href="#">Email: sports@dnatechnology.co.id</a>
                    </li>
                </ul>
            </div>
            <!-- TOP FIXED MENU -->
            <div class="i-head-right i-head-com col-md-6 col-sm-12 col-xs-12">
                <ul>
                    <li class="top-scal"><a href="booking.html"><i class="fa fa-ticket" aria-hidden="true"></i> Ticket Booking</a>
                    </li>
                    <li class="top-scal-1"><a href="event-register.html"><i class="fa fa-registered" aria-hidden="true"></i> Event Register</a>
                    </li>
                    <li><a href="#" class="tr-menu"><i class="fa fa-chevron-down" aria-hidden="true"></i> Browse</a>
                        <div class="cat-menu">

                            <div class="col-md-3 col-sm-6 cm1 mob-hid">
                                <h4>Popular Sports Category</h4>
                                <ul>
                                    <li><a href="football.html"><i class="fa fa-angle-right" aria-hidden="true"></i> american football</a>
                                    </li>
                                    <li><a href="soccer.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Soccer davidson</a>
                                    </li>
                                    <li><a href="cricket.html"><i class="fa fa-angle-right" aria-hidden="true"></i> cricket league</a>
                                    </li>
                                    <li><a href="tennis.html"><i class="fa fa-angle-right" aria-hidden="true"></i> tennis davidson</a>
                                    </li>
                                    <li><a href="volleyball.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Volleyball Champions</a>
                                    </li>
                                    <li><a href="training.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Fitness Training</a>
                                    </li>
                                    <li><a href="all-sports.html"><i class="fa fa-angle-right" aria-hidden="true"></i> View All Sports</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3 col-sm-6 cm1 mob-hid">
                                <h4>current events</h4>
                                <ul>
                                    <li><a href="soccer.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Soccer Coaching Under 18</a>
                                    </li>
                                    <li><a href="yoga.html"><i class="fa fa-angle-right" aria-hidden="true"></i> yoga & fitness training classes</a>
                                    </li>
                                    <li><a href="body-building.html"><i class="fa fa-angle-right" aria-hidden="true"></i> bodybuilding 2019</a>
                                    </li>
                                    <li><a href="surfing.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Hawaii Surfing Championship</a>
                                    </li>
                                    <li><a href="golf.html"><i class="fa fa-angle-right" aria-hidden="true"></i> european golf 2019 </a>
                                    </li>
                                    <li><a href="cricket.html"><i class="fa fa-angle-right" aria-hidden="true"></i> cricket champions league</a>
                                    </li>
                                    <li><a href="events.html"><i class="fa fa-angle-right" aria-hidden="true"></i> View All Events</a>
                                    </li>
                                </ul>

                            </div>
                            <div class="col-md-6">
                                <h4>2019 upcoming league matches</h4>
                                <div class="foot-pop top-me-ev">
                                    <ul>
                                        <li>
                                            <a href="football.html">
                                                <img src="images/trends/2.jpeg" alt="">
                                                <div class="foot-pop-eve top-me-text">
                                                    <span>Soccer: AUGUST 23RD, 2019</span>
                                                    <h4>Football:THIS SATURDAY STARTS THE INTENSIVE TRAINING FOR THE FINAL,NEW YORK</h4>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="body-building.html">
                                                <img src="images/trends/5.jpg" alt="">
                                                <div class="foot-pop-eve top-me-text">
                                                    <span>BoduBuilding: AUGUST 23RD, 2019</span>
                                                    <h4>BoduBuilding:JAKE DRIBBLER ANNOUNCED THAT HE IS RETIRING NEXT MNONTH</h4>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="tennis.html">
                                                <img src="images/trends/6.jpeg" alt="">
                                                <div class="foot-pop-eve top-me-text">
                                                    <span>BASKETBALL: AUGUST 23RD, 2019</span>
                                                    <h4>BASKETBALL:THE ALCHEMISTS NEWS COACH IS BRINGIN A NEW SHOOTING GUARD</h4>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                            <div class="cbb2-home-nav-bot mob-hid">
                                <ul>
                                    <li>One of the Best Sports website template forever <span>Call us on: +01 3214 6581</span>
                                    </li>
                                    <li><a href="javascript:void(0);" class="cbb2-ani-btn-join"><i class="fa fa-user" aria-hidden="true"></i> Account</a>
                                    </li>
                                    <li>
                                        <a href="join-our-club.html" class="cbb2-ani-btn"><i class="fa fa-life-buoy" aria-hidden="true"></i> Join Our Club</a>
                                    </li>
                                    <li><a href="join-club.html" class="cbb2-ani-btn"><i class="fa fa-dollar" aria-hidden="true"></i> Make Donation</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </li>
                </ul>
            </div>
        </div>
    </section>

    <!-- SECTION: HEADER TOP -->
    <section>
        <div class="page-head">
            <div class="lp inn-head-pad">
                <div class="row">
                    <div class="col-md-4 top-head">
                        <a href="index.html"><img src="images/logo.png" alt="">
                        </a>
                    </div>
                    <div class="col-md-3 top-search">
                        <form>
                            <ul>
                                <li>
                                    <input type="text" placeholder="Search Here..">
                                </li>
                                <li>
                                    <input type="submit" value="search">
                                </li>
                            </ul>
                        </form>
                    </div>
                    <div class="col-md-5">
                        <ul class="top-soc">
                            <li>
                                <h4>Follow Us : </h4>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook fb1"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-twitter tw1"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-google-plus gp1"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-whatsapp wa1"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-envelope-o sh1"></i></a>
                            </li>
                        </ul>
                        <ul class="brea-top">
                            <li><a href="#">Breadcrumb:</a>
                            </li>
                            <li><a href="#">Home</a>
                            </li>
                            <li><a href="#" class="brea-act">Training</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- REGISTER MY INFORMATION -->
    <section>
        <div class="booking-bg-s lp">
            <div class="booking-bg-1">
                <!-- <nav>
                    <ol class="cd-multi-steps text-bottom count">
                        <li class="visited"><a href="event-register.html">Register Tournament</a></li>
                        <li class="visited"><a href="event-kal-junior.html">Kalender Penyelenggara</a></li>
                        <li class="current"><a href="event-size-junior.html">Size of Draw</a></li>
                        <li><em>Lokasi Pertandingan</em></li>
                        <li><em>Fasilitas Penunjang</em></li>
                        <li><em>Perangkat Pertandingan</em></li>
                        <li><em>Info Pendaftaran</em></li>
                        <li><em>Biaya Pendaftaran</em></li>
                        <li><em>Konfirmasi Pembayaran</em></li>
                    </ol>
                </nav> -->
                <div class="bg-book">
                    <div class="spe-title-1 spe-title-wid">
                        <!-- <h2>Register your <span>Event Now!</span> </h2> -->
                        <h2>Size <span>Of Draw</span> </h2>
                        <div class="hom-tit">
                            <div class="hom-tit-1"></div>
                            <div class="hom-tit-2"></div>
                            <div class="hom-tit-3"></div>
                        </div>
                        <!-- <p>Feel the thrill of seeing a global sporting event in one of the world's most incredible cities. Headlining the calendar is the Dubai World Cup</p> -->
                    </div>
                    <!-- <div class="book-succ">Thank you for Register your Event with us.</div> -->
                    <div class="book-form">
                        <form class="form-horizontal" id="er_form" name="er_form" action="mail/er.php">
                            <div class="form-group">
                                <div class="col-sm-10">
                                    <label class="radio-inline">
                                        <input type="radio" value="umum" checked>Umum
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- Babak Kualifikasi -->
                                <label class="control-label col-sm-12" style="text-align: left; font-size: 18px;">Babak Kualifikasi</label>
                                <div class="clearfix"></div>
                                <!-- Putra -->
                                <div style="margin: 10px 0px;">
                                    <label class="control-label col-sm-12" style="text-align: left; font-size: 16px;">Putra</label>
                                    <div class="clearfix"></div>
                                    <label class="control-label col-sm-2">Tunggal</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                    </div>
                                    <label class="control-label col-sm-2">Ganda</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <!-- Putri -->
                                <div style="margin: 10px 0px;">
                                    <label class="control-label col-sm-12" style="text-align: left; font-size: 16px;">Putri</label>
                                    <div class="clearfix"></div>
                                    <label class="control-label col-sm-2">Tunggal</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                    </div>
                                    <label class="control-label col-sm-2">Ganda</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                    </div>
                                </div>

                                <!-- Babak Utama -->
                                <label class="control-label col-sm-12" style="text-align: left; font-size: 18px;">Babak Utama</label>
                                <div class="clearfix"></div>
                                <!-- Putra -->
                                <div style="margin: 10px 0px;">
                                    <label class="control-label col-sm-12" style="text-align: left; font-size: 16px;">Putra</label>
                                    <div class="clearfix"></div>
                                    <label class="control-label col-sm-2">Tunggal</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                    </div>
                                    <label class="control-label col-sm-2">Ganda</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <!-- Putri -->
                                <div style="margin: 10px 0px;">
                                    <label class="control-label col-sm-12" style="text-align: left; font-size: 16px;">Putri</label>
                                    <div class="clearfix"></div>
                                    <label class="control-label col-sm-2">Tunggal</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                    </div>
                                    <label class="control-label col-sm-2">Ganda</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                    </div>
                                </div>

                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-offset-6 col-sm-3">
                                    <input type="submit" id="back" value="Back">
                                </div>
                                <div class="col-sm-3">
                                    <input type="submit" id="next" value="Next">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--SECTION: BLOG POSTS-->
    

    <!--SECTION: FOOTER-->
    <section>
        <div class="ffoot">
            <div class="lp">
                <!--SECTION: FOOTER-->
                <div class="row">
                    <div class="col-md-12 foot1">
                        <a href="#"><img src="images/logo.png" alt="">
                        </a>
                        <ul>
                            <li><span>10,231,124</span> Community Members</li>
                            <li><span>512</span> Events</li>
                            <li><span>2124</span> News</li>
                        </ul>
                    </div>
                </div>
                <!--SECTION: FOOTER-->
                <div class="row foot2">
                    <div class="col-md-3">
                        <div class="foot2-1 foot-com">
                            <h4>VISI</h4>
                            <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A. Landmark : Next To Airport</p>
                        </div>
                        <div class="foot2-1 foot-com">
                            <h4>MISI</h4>
                            <p>No 3241, Grandiz Street Towers, Desay City, U.A.E. Landmark : Next To Grand Hotel</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="foot2-1 foot-com">
                            <h4>LATAR BELAKANG</h4>
                            <p>No 3241, Grandiz Street Towers, Desay City, U.A.E. Landmark : Next To Grand Hotel</p>
                        </div>
                        <div class="foot2-2 foot-soc foot-com">
                            <h4>Follow Us Now</h4>
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook fb1"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-twitter tw1"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-google-plus gp1"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-whatsapp wa1"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-envelope-o sh1"></i></a>
                                </li>
                            </ul>
                            <span class="foot-ph">Phone: +68 215707203</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="foot2-32 foot-pop foot-com">
                            <h4>MAP</h4>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3642.9719549633164!2d106.80079240702739!3d-6.220336860778754!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f14b5e389f4f%3A0x5bfd802500992d10!2sTennis+Indoor+Stadium!5e0!3m2!1sid!2sid!4v1558972920352!5m2!1sid!2sid" width="527" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Footer-->
    <!-- Sponsor -->
    <section>
        <div class="ffoot sponsor">
            <div class="lp">
                <div class="row">
                    <div class="col-md-12 foot4">
                        <h5>Our Sponsors</h5>
                        <ul>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-bri.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-totalindo.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-kapalapi.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-wika.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-harumenergy.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-abipraya.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-head.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-pp.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-bni.jpg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="#"><img src="https://pelti.or.id/wp-content/uploads/2018/12/logo-widyachandra.jpg" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <!-- FOOTER: COPY RIGHTS -->
        <div class="fcopy">
            <div class="lp copy-ri row">
                <div class="col-md-6 col-sm-12 col-xs-12">Supported by DNA TECHNOLOGY</div>
            </div>
        </div>
    </section>

    <!--== Bootstrap & Latest JS ==-->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script src="js/custom.js"></script>
    <script>
        $(document).ready(() => {
            $('#next').on('click', (e) => {
                window.location = "/event-locations.html";
            })
            $('#back').on('click', (e) => {
                window.location = "/event-size-junior.html";
            })
        });
    </script>
</body>

</html>