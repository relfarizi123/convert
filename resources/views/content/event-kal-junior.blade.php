@extends('layout.master')

@section('content')

@include('layout.header-search')
    <!-- REGISTER MY INFORMATION -->
    <section>
        <div class="booking-bg-s lp">
            <div class="booking-bg-1">
                <!-- <nav>
                    <ol class="cd-multi-steps text-bottom count">
                        <li class="visited"><a href="event-register.html">Register Tournament</a></li>
                        <li class="current"><a href="event-kal-junior.html">Kalender Penyelenggara</a></li>
                        <li><em>Size of Draw</em></li>
                        <li><em>Lokasi Pertandingan</em></li>
                        <li><em>Fasilitas Penunjang</em></li>
                        <li><em>Perangkat Pertandingan</em></li>
                        <li><em>Info Pendaftaran</em></li>
                        <li><em>Biaya Pendaftaran</em></li>
                        <li><em>Konfirmasi Pembayaran</em></li>
                    </ol>
                </nav> -->
                <div class="bg-book">
                    <div class="spe-title-1 spe-title-wid">
                        <!-- <h2>Register your <span>Event Now!</span> </h2> -->
                        <h2>Kalender <span>Penyelengaraan</span> </h2>
                        <div class="hom-tit">
                            <div class="hom-tit-1"></div>
                            <div class="hom-tit-2"></div>
                            <div class="hom-tit-3"></div>
                        </div>
                        <!-- <p>Feel the thrill of seeing a global sporting event in one of the world's most incredible cities. Headlining the calendar is the Dubai World Cup</p> -->
                    </div>
                    <!-- <div class="book-succ">Thank you for Register your Event with us.</div> -->
                    <div class="book-form">
                        <form class="form-horizontal" id="er_form" name="er_form" action="mail/er.php">
                            <div class="form-group">
                                <div class="col-sm-10">
                                    <label class="radio-inline kat-umur">
                                        <input type="radio" name="umur" value="max-18" checked>KU 18
                                    </label>
                                    <label class="radio-inline kat-umur">
                                        <input type="radio" name="umur" value="max-16" >KU 16
                                    </label>
                                    <label class="radio-inline kat-umur">
                                        <input type="radio" name="umur" value="max-14" >KU 14
                                    </label>
                                    <label class="radio-inline kat-umur">
                                        <input type="radio" name="umur" value="max-12" >KU 12
                                    </label>
                                    <label class="radio-inline kat-umur">
                                        <input type="radio" name="umur" value="max-10" >KU 10
                                    </label>
                                </div>
                            </div>
                            <!-- Kurang Dari 18 Tahun -->
                            <div class="max-18">
                                <div class="form-group">
                                    <!-- Putra -->
                                    <label class="control-label col-sm-12 left" style="text-align: center; font-size: 18px;">Putra</label>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Kualifikasi</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Babak Utama</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                    <!-- Putri -->
                                    <label class="control-label col-sm-12 left" style="text-align: center; font-size: 18px;">Putri</label>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Kualifikasi</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Babak Utama</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Kurang Dari 16 Tahun -->
                            <div class="max-16 hidden">
                                <div class="form-group">
                                    <!-- Putra -->
                                    <label class="control-label col-sm-12 left" style="text-align: center; font-size: 18px;">Putra</label>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Kualifikasi</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Babak Utama</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                    <!-- Putri -->
                                    <label class="control-label col-sm-12 left" style="text-align: center; font-size: 18px;">Putri</label>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Babak Utama</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Babak Utama</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Kurang Dari 14 Tahun -->
                            <div class="max-14 hidden">
                                <div class="form-group">
                                    <!-- Putra -->
                                    <label class="control-label col-sm-12 left" style="text-align: center; font-size: 18px;">Putra</label>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Kualifikasi</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Babak Utama</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                    <!-- Putri -->
                                    <label class="control-label col-sm-12 left" style="text-align: center; font-size: 18px;">Putri</label>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Babak Utama</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Babak Utama</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Kurang Dari 12 Tahun -->
                            <div class="max-12 hidden">
                                <div class="form-group">
                                    <!-- Putra -->
                                    <label class="control-label col-sm-12 left" style="text-align: center; font-size: 18px;">Putra</label>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Kualifikasi</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Babak Utama</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                    <!-- Putri -->
                                    <label class="control-label col-sm-12 left" style="text-align: center; font-size: 18px;">Putri</label>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Babak Utama</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Babak Utama</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Kurang Dari 10 Tahun -->
                            <div class="max-10 hidden">
                                <div class="form-group">
                                    <!-- Putra -->
                                    <label class="control-label col-sm-12 left" style="text-align: center; font-size: 18px;">Putra</label>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Kualifikasi</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Babak Utama</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                    <!-- Putri -->
                                    <label class="control-label col-sm-12 left" style="text-align: center; font-size: 18px;">Putri</label>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Babak Utama</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="margin: 10px 0px;">
                                        <label class="control-label col-sm-3" style="text-align: left;">Babak Utama</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                        <label class="control-label col-sm-1" style="text-align: center;">S.D</label>
                                        <div class="col-sm-4">
                                            <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-offset-6 col-sm-3">
                                    <input type="submit" id="back" value="Back">
                                </div>
                                <div class="col-sm-3">
                                    <input type="submit" id="next" value="Next">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!--== Bootstrap & Latest JS ==-->
    <script>
        $(document).ready(() => {
            $('#next').on('click', (e) => {
                window.location = "/event-register/event-kal-junior/event-size-junior";
            })
            $('#back').on('click', (e) => {
                window.location = "/event-register";
            })
            
            $('.kat-umur input').on('click', (e) => {
                let block = e.target.value;
                switch(block) {
                    case "max-18":
                        $('.max-18').removeClass('hidden');
                        $('.max-16').addClass('hidden');
                        $('.max-14').addClass('hidden');
                        $('.max-12').addClass('hidden');
                        $('.max-10').addClass('hidden');
                        break;
                    case "max-16":
                        $('.max-16').removeClass('hidden');
                        $('.max-18').addClass('hidden');
                        $('.max-14').addClass('hidden');
                        $('.max-12').addClass('hidden');
                        $('.max-10').addClass('hidden');
                        break;
                    case "max-14":
                        $('.max-14').removeClass('hidden');
                        $('.max-18').addClass('hidden');
                        $('.max-16').addClass('hidden');
                        $('.max-12').addClass('hidden');
                        $('.max-10').addClass('hidden');
                        break;
                    case "max-12":
                        $('.max-12').removeClass('hidden');
                        $('.max-18').addClass('hidden');
                        $('.max-14').addClass('hidden');
                        $('.max-16').addClass('hidden');
                        $('.max-10').addClass('hidden');
                        break;
                    case "max-10":
                        $('.max-10').removeClass('hidden');
                        $('.max-18').addClass('hidden');
                        $('.max-14').addClass('hidden');
                        $('.max-12').addClass('hidden');
                        $('.max-16').addClass('hidden');
                        break;
                    default:
                        $('.max-18').removeClass('hidden');
                        $('.max-16').addClass('hidden');
                        $('.max-14').addClass('hidden');
                        $('.max-12').addClass('hidden');
                        $('.max-10').addClass('hidden');
                        break;
                }
            })
        });
    </script>

@include('layout.footer')
@endsection
