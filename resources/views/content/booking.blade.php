@extends('layout.master')

@section('content')

<!-- SECTION: HEADER TOP -->

<header>
@include('layout.header-search')
</header>

<!-- REGISTER MY INFORMATION -->
<section>
    <div class="booking-bg-s tic-book-bg lp">
        <div class="booking-bg-1">
            <div class="bg-book">
                <div class="spe-title-1 spe-title-wid">
                    <h2>Ticket Booking <span>Opens Now!</span> </h2>
                    <div class="hom-tit">
                        <div class="hom-tit-1"></div>
                        <div class="hom-tit-2"></div>
                        <div class="hom-tit-3"></div>
                    </div>
                    <p>Feel the thrill of seeing a global sporting event in one of the world's most incredible cities. Headlining the calendar is the Dubai World Cup</p>
                </div>
                <div class="book-succ">Thank you for booking with us.</div>
                <div class="book-form">
                    <form id="b_form" name="b_form" class="form-horizontal" action="mail/booking.php">
                        <div class="form-group">
                            <label class="control-label col-sm-2">Name</label>
                            <div class="col-sm-10">
                                <input type="text" id="bname" name="bname" class="form-control" placeholder="Type your name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">E-mail</label>
                            <div class="col-sm-10">
                                <input type="email" id="bmail" name="bmail" class="form-control" placeholder="Type your email" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Number</label>
                            <div class="col-sm-10">
                                <input type="number" id="bphone" name="bphone" class="form-control" placeholder="Type your phone" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Date</label>
                            <div class="col-sm-10">
                                <input type="date" id="bdate" name="bdate" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">City</label>
                            <div class="col-sm-10">
                                <input type="text" id="bcity" name="bcity" class="form-control" placeholder="Type your city" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Address</label>
                            <div class="col-sm-10">
                                <textarea id="baddress" name="baddress" class="form-control" placeholder="Type your address"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="submit" value="submit" id="send_button">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!--SECTION: BLOG POSTS-->
<section>
    <div class="blog row">
        <div class="lp">
            <!-- BLOG POST: POST DATE -->
            <div class="blog-1 col-md-2">
                <span>Latest Posts</span>
                <h4>25</h4>
                <span>Augest 2017</span>
            </div>
            <!-- BLOG POST: POST NAME & DESCRIPTION -->
            <div class="blog-2 col-md-8">
                <ul>
                    <li>
                        <a href="#">
                            <h4>WESTERN SYDNEY WANDERERS VS URAWA RED DIAMONDS</h4>
                        </a>
                    </li>
                    <li>
                        <p>In efficitur nisi et condimentum mattis. Duis et aliquet purus, quis congue elit. Cras volutpat dapibus molestie. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec posuere mollis augue, a accumsan libero egestas sit amet.Vestibulum posuere erat tortor, porta tempus leo condimentum sed. </p>
                    </li>
                </ul>
            </div>
            <!-- BLOG POST: POST COMMENTS,TAG AND SOCIAL MEDIA -->
            <div class="blog-3 col-md-2">
                <ul>
                    <li><i class="fa fa-comment-o" aria-hidden="true"></i> Comments</li>
                    <li><i class="fa fa-tag" aria-hidden="true"></i> Tag</li>
                    <li><i class="fa fa-share-alt" aria-hidden="true"></i> Share This</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--SECTION: FOOTER-->
@include('layout.footer')
@endsection