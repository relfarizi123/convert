@extends('layout.master')
@section('content')

<

    <!-- SECTION: HEADER TOP -->
   
    <!-- REGISTER MY INFORMATION -->
    <section>
        <div class="booking-bg-s lp">
                <!-- <nav>
                    <ol class="cd-multi-steps text-bottom count">
                        <li class="current"><a href="event-register.html"></a></li>
                        <li><em></em></li>
                        <li><em></em></li>
                        <li><em></em></li>
                        <li><em></em></li>
                        <li><em></em></li>
                        <li><em></em></li>
                        <li><em></em></li>
                        <li><em></em></li>
                    </ol>
                </nav> -->
            <div class="booking-bg-1">
                <div class="bg-book">
                    <div class="spe-title-1 spe-title-wid">
                        <!-- <h2>Register your <span>Event Now!</span> </h2> -->
                        <h2>Register <span>Tournament</span> </h2>
                        <div class="hom-tit">
                            <div class="hom-tit-1"></div>
                            <div class="hom-tit-2"></div>
                            <div class="hom-tit-3"></div>
                        </div>
                        <!-- <p>Feel the thrill of seeing a global sporting event in one of the world's most incredible cities. Headlining the calendar is the Dubai World Cup</p> -->
                    </div>
                    <!-- <div class="book-succ">Thank you for Register your Event with us.</div> -->
                    <div class="book-succ">Data submitted</div>
                    <div class="book-form">
                        <form class="form-horizontal" id="er_form" name="er_form" action="mail/er.php">
                            <div class="form-group">
                                <label class="control-label col-sm-2">Nama Turnamen</label>
                                <div class="col-sm-10">
                                    <input type="text" id="ername" name="ername" class="form-control" placeholder="Type tournament name" value="Tennis" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Kategori Peserta</label>
                                <div class="col-sm-10">
                                    <label class="checkbox-inline"><input type="checkbox" name="kategori">Putra</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="kategori">Putri</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Jenis Pertandingan</label>
                                <div class="col-sm-10">
                                    <label class="checkbox-inline" style="padding-left:16px;"><input type="checkbox" name="jenis">Tunggal</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="jenis">Ganda</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Kelompok Peserta</label>
                                <div class="col-sm-10">
                                    <label class="checkbox-inline checkbox-kelompok-junior"><input type="radio" value="junior" name="kelompok">Junior</label>
                                    <label class="checkbox-inline checkbox-kelompok-umum"><input type="radio" value="umum" name="kelompok">Umum</label>
                                    <div class="kelompok-junior-range hidden" style="margin: 10px 0">
                                        <label class="checkbox-inline"><input type="checkbox" value="ku18">KU 18</label>
                                        <label class="checkbox-inline"><input type="checkbox" value="ku16">KU 16</label>
                                        <label class="checkbox-inline"><input type="checkbox" value="ku14">KU 14</label>
                                        <label class="checkbox-inline"><input type="checkbox" value="ku12">KU 12</label>
                                        <label class="checkbox-inline"><input type="checkbox" value="ku10">KU 10</label>
                                    </div>
                                    <div class="kelompok-umum-range hidden" style="margin: 10px 0;">
                                        <h3 style="text-align: center">HADIAH UANG</h3>
                                        <input type="text" class="form-control" placeholder="IDR" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2">Tanggal Mulai</label>
                                <div class="col-sm-10">
                                    <input type="date" id="erdate" name="erdate" class="form-control" value="2019-08-01" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Tanggal Berakhir</label>
                                <div class="col-sm-10">
                                    <input type="date" id="erdate" name="erdate" class="form-control" value="2019-10-30" required>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <input type="submit" id="next" value="Next">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--SECTION: BLOG POSTS-->
    
    <!--SECTION: FOOTER-->
    @include('layout.footer')
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/custom.js')} "></script>
    <script>
        $(document).ready(() => {
            $('.checkbox-kelompok-junior').on('click', (e) => {
                if (e.target.checked == true){
                    $('.kelompok-junior-range').removeClass('hidden');
                }else{
                    $('.kelompok-junior-range').addClass('hidden');
                }
            })
            $('.checkbox-kelompok-umum').on('click', (e) => {
                if (e.target.checked == true){
                    $('.kelompok-umum-range').removeClass('hidden');
                }else{
                    $('.kelompok-umum-range').addClass('hidden');
                }
            })

            $('#next').on('click', (e) => {
                window.location = "event-register/event-kal-umum";
            })
        });
    </script>
@endsection
