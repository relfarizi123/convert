<section>
        <div class="page-head">
            <div class="lp inn-head-pad">
                <div class="row">
                    <div class="col-md-4 top-head">
                        <a href="/"><img src="{{ asset('images/logo.png') }}" alt="">
                        </a>
                    </div>
                    <div class="col-md-3 top-search">
                        <form>
                            <ul>
                                <li>
                                    <input type="text" placeholder="Search Here..">
                                </li>
                                <li>
                                    <input type="submit" value="search">
                                </li>
                            </ul>
                        </form>
                    </div>
                    <div class="col-md-5">
                        <ul class="top-soc">
                            <li>
                                <h4>Follow Us : </h4>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook fb1"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-twitter tw1"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-google-plus gp1"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-whatsapp wa1"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-envelope-o sh1"></i></a>
                            </li>
                        </ul>
                        <ul class="brea-top">
                            <li><a href="#">Breadcrumb:</a>
                            </li>
                            <li><a href="#">Home</a>
                            </li>
                            <li><a href="#" class="brea-act">Training</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>