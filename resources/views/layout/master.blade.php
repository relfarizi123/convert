<!DOCTYPE html>
<html lang="en">

<head>
    <title>Real Sports Club: Soccer,Football,Cricket,Water Sports,Fitness,Yoga and All Sports Events</title>
    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- FAV ICON(BROWSER TAB ICON) -->
    <link rel="shortcut icon" href="images/fav.ico" type="image/x-icon">

    <!-- GOOGLE FONT -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700%7CMontserrat:300,500%7COswald:400,500" rel="stylesheet">

    <!-- FONTAWESOME ICONS -->
    <!-- ALL CSS FILES -->
    <link rel="stylesheet" href="{{ asset('css/all-style.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>

    <section>
        <!-- LEFT SIDE NAVIGATION MENU -->
        <div class="menu">
                <ul>
                    <!-- MAIN MENU -->
                    <li>
                        <a href="/"><img src= "{{ asset('images/icon/s1.png') }}" alt=""> Home</a>
                    </li>
                    <li>
                        <a href="/about"><img src="{{ asset('images/menu/about.png') }}" alt=""> About</a>
                    </li>
                    <li>
                        <a href="/tournamnet"><img src="{{ asset('images/menu/turnament.png') }}" alt=""> Tournament</a>
                    </li>
                    <li>
                        <a href="/rangking"><img src="{{ asset('images/menu/rangking.png') }}" alt=""> Rangking</a>
                    </li>
                    <li>
                        <a href="/player"><img src="{{ asset('images/menu/player.png') }}" alt=""> Player</a>
                    </li>
                    <li>
                        <a href="/news"><img src="{{ asset('images/menu/news.png') }}" alt=""> News</a>
                    </li>
                    <li>
                        <a href="/gallery"><img src="{{ asset('images/menu/galerry.png') }}" alt=""> Gallery</a>
                    </li>
                    <li>
                        <a href="/ayo-tennis"><img src="{{ asset('images/menu/ayotennis.png') }}" alt=""> Ayo Tennis</a>
                    </li>
                    <li>
                        <a href="/club"><img src="{{ asset('images/menu/club.png') }}" alt=""> Club</a>
                    </li>
                    <li>
                        <a href="/contact-us"><img src="{{ asset('images/icon/contact.png') }}" alt=""> Contact US</a>
                    </li>
                    <li>
                        <a href="/login"><img src="{{ asset('images/icon/about.png') }}" alt=""> Login</a>
                    </li>
                </ul>
        </div>
        

        <!-- RIGHT SIDE NAVIGATION MENU -->
        <!-- MOBILE MENU(This mobile menu show on 0px to 767px windows only) -->
        <div class="mob-menu">
            <span><i class="fa fa-bars" aria-hidden="true"></i></span>
        </div>
        <div class="mob-close">
            <span><i class="fa fa-times" aria-hidden="true"></i></span>
        </div>
    </section>

    <!-- TOP BAR -PHONE EMAIL AND TOP MENU -->
    <section class="i-head-top">
        <div class="i-head row">
            <!-- TOP CONTACT INFO -->
            <div class="i-head-left i-head-com col-md-6">
                <ul>
                    <li><a href="#">phone: +62 6514 7415</a>
                    </li>
                    <li><a href="#">Email: sports@dnatechnology.co.id</a>
                    </li>
                </ul>
            </div>
            <!-- TOP FIXED MENU -->
            <div class="i-head-right i-head-com col-md-6 col-sm-12 col-xs-12">
                <ul>
                    <li class="top-scal"><a href="booking"><i class="fa fa-ticket" aria-hidden="true"></i> Ticket Booking</a>
                    </li>
                    <li class="top-scal-1"><a href="{{ url('/event-register') }}"><i class="fa fa-registered" aria-hidden="true"></i> Event Register</a>
                    </li>
                    <li><a href="#" class="tr-menu"><i class="fa fa-chevron-down" aria-hidden="true"></i> Browse</a>
                        <div class="cat-menu">

                            <div class="col-md-3 col-sm-6 cm1 mob-hid">
                                <h4>Popular Sports Category</h4>
                                <ul>
                                    <li><a href="football.html"><i class="fa fa-angle-right" aria-hidden="true"></i> american football</a>
                                    </li>
                                    <li><a href="soccer.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Soccer davidson</a>
                                    </li>
                                    <li><a href="cricket.html"><i class="fa fa-angle-right" aria-hidden="true"></i> cricket league</a>
                                    </li>
                                    <li><a href="tennis.html"><i class="fa fa-angle-right" aria-hidden="true"></i> tennis davidson</a>
                                    </li>
                                    <li><a href="volleyball.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Volleyball Champions</a>
                                    </li>
                                    <li><a href="training.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Fitness Training</a>
                                    </li>
                                    <li><a href="all-sports.html"><i class="fa fa-angle-right" aria-hidden="true"></i> View All Sports</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3 col-sm-6 cm1 mob-hid">
                                <h4>current events</h4>
                                <ul>
                                    <li><a href="soccer.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Soccer Coaching Under 18</a>
                                    </li>
                                    <li><a href="yoga.html"><i class="fa fa-angle-right" aria-hidden="true"></i> yoga & fitness training classes</a>
                                    </li>
                                    <li><a href="body-building.html"><i class="fa fa-angle-right" aria-hidden="true"></i> bodybuilding 2019</a>
                                    </li>
                                    <li><a href="surfing.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Hawaii Surfing Championship</a>
                                    </li>
                                    <li><a href="golf.html"><i class="fa fa-angle-right" aria-hidden="true"></i> european golf 2019 </a>
                                    </li>
                                    <li><a href="cricket.html"><i class="fa fa-angle-right" aria-hidden="true"></i> cricket champions league</a>
                                    </li>
                                    <li><a href="events.html"><i class="fa fa-angle-right" aria-hidden="true"></i> View All Events</a>
                                    </li>
                                </ul>

                            </div>
                            <div class="col-md-6">
                                <h4>2019 upcoming league matches</h4>
                                <div class="foot-pop top-me-ev">
                                    <ul>
                                        <li>
                                            <a href="football.html">
                                                <img src="{{ asset('images/trends/2.jpeg') }}" alt="">
                                                <div class="foot-pop-eve top-me-text">
                                                    <span>Soccer: AUGUST 23RD, 2019</span>
                                                    <h4>Football:THIS SATURDAY STARTS THE INTENSIVE TRAINING FOR THE FINAL,NEW YORK</h4>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="body-building.html">
                                                <img src="{{ asset('images/trends/5.jpg') }}" alt="">
                                                <div class="foot-pop-eve top-me-text">
                                                    <span>BoduBuilding: AUGUST 23RD, 2019</span>
                                                    <h4>BoduBuilding:JAKE DRIBBLER ANNOUNCED THAT HE IS RETIRING NEXT MNONTH</h4>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="tennis.html">
                                                <img src="{{ asset('images/trends/6.jpeg') }}" alt="">
                                                <div class="foot-pop-eve top-me-text">
                                                    <span>BASKETBALL: AUGUST 23RD, 2019</span>
                                                    <h4>BASKETBALL:THE ALCHEMISTS NEWS COACH IS BRINGIN A NEW SHOOTING GUARD</h4>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                            <div class="cbb2-home-nav-bot mob-hid">
                                <ul>
                                    <li>One of the  Best Sports website template forever <span>Call us on: +01 3214 6581</span>
                                    </li>
                                    <li><a href="javascript:void(0);" class="cbb2-ani-btn-join"><i class="fa fa-user" aria-hidden="true"></i> Account</a>
                                    </li>
                                    <li>
                                        <a href="join-our-club.html" class="cbb2-ani-btn"><i class="fa fa-life-buoy" aria-hidden="true"></i> Join Our Club</a>
                                    </li>
                                    <li><a href="join-club.html" class="cbb2-ani-btn"><i class="fa fa-dollar" aria-hidden="true"></i> Make Donation</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </li>
                </ul>
            </div>
        </div>
    </section>
                            @yield('content')
                            
    <!--Footer-->
    <!-- Sponsor -->
        @include('layout.sponsor')

    <section>
        <!-- FOOTER: COPY RIGHTS -->
        <div class="fcopy">
            <div class="lp copy-ri row">
                <div class="col-md-6 col-sm-12 col-xs-12">Supported by DNA TECHNOLOGY</div>
            </div>
        </div>
    </section>
    <!--== Bootstrap & Latest JS ==-->
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.js')}}"></script>
    <script src="{{ asset('js/custom.js')}}" ></script>
</body>

</html>