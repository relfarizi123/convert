
<section>
        <div class="ffoot">
            <div class="lp">
                <!--SECTION: FOOTER-->
                <div class="row">
                    <div class="col-md-12 foot1">
                        <a href="#"><img src="{{ asset('images/logo.png') }}" alt="">
                        </a>
                        <ul>
                            <li><span>10,231,124</span> Community Members</li>
                            <li><span>512</span> Events</li>
                            <li><span>2124</span> News</li>
                        </ul>
                    </div>
                </div>
                <!--SECTION: FOOTER-->
                <div class="row foot2">
                    <div class="col-md-3">
                        <div class="foot2-1 foot-com">
                            <h4>VISI</h4>
                            <p>28800 Orchard Lake Road, Suite 180 Farmington Hills, U.S.A. Landmark : Next To Airport</p>
                        </div>
                        <div class="foot2-1 foot-com">
                            <h4>MISI</h4>
                            <p>No 3241, Grandiz Street Towers, Desay City, U.A.E. Landmark : Next To Grand Hotel</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="foot2-1 foot-com">
                            <h4>LATAR BELAKANG</h4>
                            <p>No 3241, Grandiz Street Towers, Desay City, U.A.E. Landmark : Next To Grand Hotel</p>
                        </div>
                        <div class="foot2-2 foot-soc foot-com">
                            <h4>Follow Us Now</h4>
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook fb1"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-twitter tw1"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-google-plus gp1"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-whatsapp wa1"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-envelope-o sh1"></i></a>
                                </li>
                            </ul>
                            <span class="foot-ph">Phone: +68 215707203</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="foot2-32 foot-pop foot-com">
                            <h4>MAP</h4>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3642.9719549633164!2d106.80079240702739!3d-6.220336860778754!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f14b5e389f4f%3A0x5bfd802500992d10!2sTennis+Indoor+Stadium!5e0!3m2!1sid!2sid!4v1558972920352!5m2!1sid!2sid" width="527" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>