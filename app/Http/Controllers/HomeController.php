<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function index(){
        return view("content.home");
    }

    public function EventRegister(){
        return view("content.event-register");
    }

    public function booking(){
        return view("content.booking");
    }

    public function about(){
        return view("content.about");
    }
    
    public function tournament(){
        return view("content.tournament");
    }
    public function EventKalJunior(){
        return view("content.event-kal-junior");
    }

    public function EventKalUmum(){
        return view("content.event-kal-umum");
    }
}
