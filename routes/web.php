<?php


Route::get('/','HomeController@index');

Route::get('/event-register', 'HomeController@EventRegister');
Route::get('event-register/event-kal-junior', 'HomeController@EventKalJunior');
Route::get('event-register/event-kal-umum', 'HomeController@EventKalUmum');
Route::get('event-register/event-kal-umum/event-size-umum', 'HomeController@EventSizeUmum');

Route::get('/booking', 'HomeController@booking');
Route::get('/about', 'HomeController@about');
Route::get('/tournament', 'HomeController@tournament');
Route::get('/rangking', 'HomeController@rangking');
Route::get('/player', 'HomeController@player');
Route::get('/news', 'HomeController@news');
Route::get('/gallery', 'HomeController@gallery');
Route::get('/ayo-tennis', 'HomeController@ayo-tennis');
Route::get('/club', 'HomeController@club');
Route::get('/contact-us', 'HomeController@contact-us');
Route::get('/login', 'HomeController@login');


